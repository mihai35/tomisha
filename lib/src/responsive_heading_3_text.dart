import 'package:flutter/material.dart';
import 'package:prototype/const/flutter_bootstrap.dart';

import '../const/colors.dart';

class ResponsiveTextWidget extends StatelessWidget {
  ResponsiveTextWidget(
    this.text, {
    super.key,
    this.fontSize,
    this.fontStyle,
    this.fontFamily,
    this.fontWeight,
    this.textColor,
    this.letterSpacing = 0.0,
    this.textAlign,
    this.textDecoration,
    this.lineHeight,
    this.decorationColor,
    this.maxLines,
    this.textOverflow,
    this.decorationThickness,
    this.responsiveFontWeight,
    this.responsiveFontSize,
    this.responsiveletterSpacing,
    this.responsiveletterTextAlign,
    this.responsiveLineHeight,
    this.style,
  });

  final String text;
  double? fontSize;
  FontWeight? fontWeight;
  Color? textColor;
  String? fontFamily;
  FontStyle? fontStyle;
  double? letterSpacing;
  TextAlign? textAlign;
  TextDecoration? textDecoration;
  double? lineHeight;
  final Color? decorationColor;
  final int? maxLines;
  final TextOverflow? textOverflow;
  final double? decorationThickness;
  final Map<DEVICES, double>? responsiveFontSize;
  final Map<DEVICES, FontWeight>? responsiveFontWeight;
  final Map<DEVICES, double>? responsiveletterSpacing;
  final Map<DEVICES, double>? responsiveLineHeight;
  final Map<DEVICES, TextAlign>? responsiveletterTextAlign;
  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    if (responsiveFontSize != null) {
      fontSize = SizeUtils.getResponsiveValue<double>(
        context,
        breakPointValues: responsiveFontSize!,
      );
    }
    if (responsiveFontWeight != null) {
      fontWeight = SizeUtils.getResponsiveValue<FontWeight>(
        context,
        breakPointValues: responsiveFontWeight!,
      );
    }

    if (responsiveletterSpacing != null) {
      letterSpacing = SizeUtils.getResponsiveValue<double>(
        context,
        breakPointValues: responsiveletterSpacing!,
      );
    }

    if (responsiveletterTextAlign != null) {
      textAlign = SizeUtils.getResponsiveValue<TextAlign>(
        context,
        breakPointValues: responsiveletterTextAlign!,
      );
    }
    if (responsiveLineHeight != null) {
      lineHeight = SizeUtils.getResponsiveValue<double>(
        context,
        breakPointValues: responsiveLineHeight!,
      );
    }
    return Text(
      text,
      maxLines: maxLines,
      textAlign: textAlign,
      style: style == null
          ? TextStyle(
              fontSize: fontSize,
              fontFamily: fontFamily,
              fontWeight: fontWeight,
              fontStyle: fontStyle,
              color: textColor ?? AppColors.lightColor.textColor1,
              letterSpacing: letterSpacing,
              height: lineHeight,
              decoration: textDecoration,
              decorationColor: decorationColor,
              overflow: textOverflow,
              decorationThickness: decorationThickness,
            )
          : style?.copyWith(
              fontSize: fontSize,
              fontFamily: fontFamily,
              fontWeight: fontWeight,
              fontStyle: fontStyle,
              color: textColor ?? AppColors.lightColor.textColor1,
              letterSpacing: letterSpacing,
              height: lineHeight,
              decoration: textDecoration,
              decorationColor: decorationColor,
              overflow: textOverflow,
              decorationThickness: decorationThickness,
            ),
    );
  }
}

class SizeUtils {
  static T getResponsiveValue<T>(BuildContext context,
      {required Map<DEVICES, T> breakPointValues}) {
    double appWidth = MediaQuery.of(context).size.width;
    String prefixString = bootstrapPrefixBasedOnWidth(appWidth);
    DEVICES device = getDeviceFromPrefixString(prefixString);

    if (breakPointValues[device] != null) {
      return breakPointValues[device]!;
    } else {
      int currentIndex = DEVICES.values.indexOf(device);
      if (currentIndex > 0) {
        for (var i = (currentIndex - 1); i >= 0; i--) {
          DEVICES d = DEVICES.values[i];
          if (breakPointValues[d] != null) {
            return breakPointValues[d]!;
          }
        }
      }
    }
    assert(true);
    throw "Set Value of needed Screens Sizes";
  }
}
